package org.gfbio.tests;

import java.util.HashMap;

import org.gfbio.enums.Ontology;
import org.gfbio.metadata.GFBioMetadataWriter;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.UnloadableImportException;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

public class WriterTestStarter {
	
	
	private static String hostURL = "jdbc:virtuoso://localhost:1111";
	private static String userName = "webservice";
	private static String password = "E6:`7k#bzeTu9'W";

	public static void main (String args []) throws UnloadableImportException, OWLOntologyCreationException {	
		VirtGraph set = new VirtGraph (hostURL, userName, password);
		
		if(args.length == 0){
		    System.out.println("Please enter the path to the folder where you stored the ontologies."
		        + "\n For example: /home/user/ontologies/");
		}
		else{
    		String base_folder_url = "file://" + args[0];
    		String befdata_url = base_folder_url+"befdata.rdf";
    		String bco_url = base_folder_url+"bco.owl";
    		String geo_url = base_folder_url+"GeographicRegion.rdf";
    		String envo_url = base_folder_url+"envo.owl";
    		String pato_url = base_folder_url+"pato.owl";
    		String chebi_url = base_folder_url+"chebi.owl";
            String qudt_url = base_folder_url+"qudt-all.owl";
            String sweet_url = base_folder_url+"sweetAll.owl";
            String ncbi_url = base_folder_url+"NCBITAXON.ttl";
    
    		/*//Clearing of the current Metadata graph
    		String text = "CLEAR GRAPH <http://terminologies.gfbio.org/terms/Metadata>";
    		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (text, set);
    		vqe.execSelect();
    */
    		GFBioMetadataWriter w1 = new GFBioMetadataWriter(befdata_url);
    		HashMap<Ontology, String> bef = new HashMap<Ontology, String>();
    		bef.put(Ontology.uri, "http://tematres.befdata.biow.uni-leipzig.de/vocab/");
    		bef.put(Ontology.name, "BEFdata");
    		bef.put(Ontology.acronym, "BEFDATA");
    		bef.put(Ontology.description, "biology");
    		bef.put(Ontology.creation_date, "01/01/2014 ");
            bef.put(Ontology.release_date, "01/01/2014 ");
    		bef.put(Ontology.ontologyLanguage, "http://terminologies.gfbio.org/terms/ontology#SKOS");
    		bef.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Biological");
            bef.put(Ontology.synonym, "http://purl.obolibrary.org/obo/hasSynonym");
            bef.put(Ontology.version, "1.0");
            bef.put(Ontology.status, "alpha");
            bef.put(Ontology.keywords, "a, ab, abc, abcd");
            bef.put(Ontology.creators, "Horst Peter, Hans Müller");
            bef.put(Ontology.contributors, "Hinz Kunz, Ursula Meier");
            bef.put(Ontology.naturalLanguage, "EN");
            bef.put(Ontology.contact_firstname, "Class-Thilo");
            bef.put(Ontology.contact_lastname, "Pfaff");
            bef.put(Ontology.contact_email, "class@thilo.com");
            bef.put(Ontology.homepage, "http://www.befdata.de");
            bef.put(Ontology.documentation, "http://www.befdata.de/docu.html");
            bef.put(Ontology.label, "http://www.w3.org/2004/02/skos/core#prefLabel");
            bef.put(Ontology.definition, "http://www.w3.org/2004/02/skos/core#definition");
            bef.put(Ontology.formality_level, "http://omv.ontoware.org/2005/05/ontology#Terminology");
            bef.put(Ontology.backwardCompatibility, "true");
            w1.insert(bef);				/*
    		//Testcode for adding BCO ontology to metadataonto
    		GFBioMetadataWriter w2 = new GFBioMetadataWriter(bco_url);
    		HashMap<Ontology, String> bco = new HashMap<Ontology, String>();
    		bco.put(Ontology.uri, "http://purl.obolibrary.org/obo/bco.owl");
    		bco.put(Ontology.name, "Biological Collections Ontology");
    		bco.put(Ontology.acronym, "BCO");
    		bco.put(Ontology.description, "The biological collection ontology includes consideration of "
    			+ "the distinctions between individuals, organisms, voucher specimens, lots, and "
    			+ "samples the relations between these entities, and processes governing the creation "
    			+ "and use of \"samples\". Within scope as well are properties including collector, "
    			+ "location, time, storage environment, containers, institution, "
    			+ "and collection identifiers. ");
    		bco.put(Ontology.creation_date, "03/29/2014 ");
    		bco.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");	
    		bco.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Biological");
    		bco.put(Ontology.definition, "http://purl.obolibrary.org/obo/IAO_0000115");
    		bco.put(Ontology.synonym, "http://purl.obolibrary.org/obo/IAO_0000118");
            bco.put(Ontology.label, "http://www.w3.org/2004/02/skos/core#prefLabel");
            bco.put(Ontology.contact_firstname, "Foo");
            bco.put(Ontology.contact_lastname, "Bar");
            bco.put(Ontology.contact_email, "foo@bar.com");
            bco.put(Ontology.backwardCompatibility, "true");
    		w2.insert(bco);
    		//Testcode for adding GEOREGION ontology to metadataonto
            GFBioMetadataWriter w3 = new GFBioMetadataWriter(geo_url);
            HashMap<Ontology, String> geo = new HashMap<Ontology, String>();
            geo.put(Ontology.uri, "http://rs.tdwg.org/ontology/voc/GeographicRegion");
            geo.put(Ontology.name, "TDWG Geographic Region Ontology");
            geo.put(Ontology.acronym, "TDWGREGION");
            geo.put(Ontology.description, "Ontology describing a controlled vocabulary for all of the "
                + "TDWG regions level 1 to 4 described at http://www.nhm.ac.uk/hosted_sites/tdwg/geo2.htm "
                + "and http://www.kew.org/gis/tdwg/ and collection identifiers. ");
            geo.put(Ontology.creation_date, "08/15/2007 ");
            geo.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Geographical");
            geo.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");
            geo.put(Ontology.definition, "http://rs.tdwg.org/ontology/Base#definition");
            geo.put(Ontology.label, "http://www.w3.org/2004/02/skos/core#prefLabel");
            geo.put(Ontology.contact_firstname, "Foo");
            geo.put(Ontology.contact_lastname, "Bar");
            geo.put(Ontology.contact_email, "foo@bar.com");
            geo.put(Ontology.backwardCompatibility, "true");
            w3.insert(geo);
            //Testcode for adding ENVO ontology to metadataonto
           GFBioMetadataWriter w4 = new GFBioMetadataWriter(envo_url);
            HashMap<Ontology, String> envo = new HashMap<Ontology, String>();
            envo.put(Ontology.uri, "http://purl.obolibrary.org/obo/envo.owl");
            envo.put(Ontology.name, "Environment Ontology");
            envo.put(Ontology.acronym, "ENVO");
            envo.put(Ontology.description, "Ontology of environmental features and habitats");
            envo.put(Ontology.creation_date, "12/21/2013 ");
            envo.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Environmental");
            envo.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");  
            envo.put(Ontology.definition, "http://purl.obolibrary.org/obo/IAO_0000115");
            envo.put(Ontology.synonym, "http://purl.obolibrary.org/obo/IAO_0000118");
            envo.put(Ontology.label, "http://www.w3.org/2004/02/skos/core#prefLabel");
            envo.put(Ontology.contact_firstname, "Foo");
            envo.put(Ontology.contact_lastname, "Bar");
            envo.put(Ontology.contact_email, "foo@bar.com");
            envo.put(Ontology.backwardCompatibility, "true");
            w4.insert(envo);
            //Testcode for adding ENVO ontology to metadataonto
            GFBioMetadataWriter w5 = new GFBioMetadataWriter(pato_url);
            HashMap<Ontology, String> pato = new HashMap<Ontology, String>();
            pato.put(Ontology.uri, "http://purl.obolibrary.org/obo/pato.owl");
            pato.put(Ontology.name, "Phenotypic Quality Ontology");
            pato.put(Ontology.acronym, "PATO");
            pato.put(Ontology.description, "Phenotypic qualities (properties). This ontology can be "
                + "used in conjunction with other ontologies such as GO or anatomical ontologies to "
                + "refer to phenotypes. Examples of qualities are red, ectopic, high temperature, "
                + "fused, small, edematous and arrested. ");
            pato.put(Ontology.creation_date, "04/09/2014 ");
            pato.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Anatomical");
            pato.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");  
            pato.put(Ontology.definition, "http://purl.obolibrary.org/obo/IAO_0000115");
            pato.put(Ontology.synonym, "http://purl.obolibrary.org/obo/IAO_0000118");
            pato.put(Ontology.label, "http://www.w3.org/2004/02/skos/core#prefLabel");
            pato.put(Ontology.contact_firstname, "Foo");
            pato.put(Ontology.contact_lastname, "Bar");
            pato.put(Ontology.contact_email, "foo@bar.com");
            pato.put(Ontology.backwardCompatibility, "true");
            w5.insert(pato);
            //Testcode for adding CHEBI ontology to metadataonto
            GFBioMetadataWriter w6 = new GFBioMetadataWriter(chebi_url);
            HashMap<Ontology, String> chebi = new HashMap<Ontology, String>();
            chebi.put(Ontology.uri, "http://purl.obolibrary.org/obo/chebi.owl");
            chebi.put(Ontology.name, "Chemical Entities of Biological Interest Ontology");
            chebi.put(Ontology.acronym, "CHEBI");
            chebi.put(Ontology.description, "A structured classification of chemical compounds of biological relevance.");
            chebi.put(Ontology.creation_date, "08/31/2014");
            chebi.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Chemical");
            chebi.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");  
            chebi.put(Ontology.definition, "http://purl.obolibrary.org/obo#Definition");
            chebi.put(Ontology.synonym, "http://purl.obolibrary.org/obo#Synonym");
            chebi.put(Ontology.label, "http://www.w3.org/2004/02/skos/core#prefLabel");
            chebi.put(Ontology.contact_firstname, "Foo");
            chebi.put(Ontology.contact_lastname, "Bar");
            chebi.put(Ontology.contact_email, "foo@bar.com");
            chebi.put(Ontology.backwardCompatibility, "true");
            w6.insert(chebi);
            GFBioMetadataWriter w7 = new GFBioMetadataWriter(qudt_url);
            HashMap<Ontology, String> qudt = new HashMap<Ontology, String>();
            qudt.put(Ontology.uri, "http://data.qudt.org/qudt/owl/1.0.0/qudt-all.owl");
            qudt.put(Ontology.name, "Quantities, Units, Dimensions and Data Types Ontologies");
            qudt.put(Ontology.acronym, "QUDT");
            qudt.put(Ontology.description, "The QUDT Ontologies, and derived XML Vocabularies, are being developed by TopQuadrant and NASA. Originally, they were developed for the NASA Exploration Initiatives Ontology Models (NExIOM) project, a Constellation Program initiative at the AMES Research Center (ARC). They now for the basis of the NASA QUDT Handbook to be published by NASA Headquarters. ");
            qudt.put(Ontology.creation_date, "03/18/2014");
            qudt.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Chemical");
            qudt.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");  
            qudt.put(Ontology.label, "http://www.w3.org/2000/01/rdf-schema#label");
            qudt.put(Ontology.definition, "http://purl.obolibrary.org/obo#Definition");
            qudt.put(Ontology.contact_firstname, "Foo");
            qudt.put(Ontology.contact_lastname, "Bar");
            qudt.put(Ontology.contact_email, "foo@bar.com");
            qudt.put(Ontology.synonym, "http://purl.obolibrary.org/obo/hasSynonym");
            qudt.put(Ontology.backwardCompatibility, "true");
            w7.insert(qudt);
            
            GFBioMetadataWriter w8 = new GFBioMetadataWriter(sweet_url);
            HashMap<Ontology, String> sweet = new HashMap<Ontology, String>();
            sweet.put(Ontology.uri, "http://sweet.jpl.nasa.gov/2.3/sweetAll.owl");
            sweet.put(Ontology.name, "Semantic Web for Earth and Environmental Terminology");
            sweet.put(Ontology.acronym, "SWEET");
            sweet.put(Ontology.description, "SWEET ontologies are written in the OWL ontology language and are publicly available. SWEET 2.3 is highly modular with 6000 concepts in 200 separate ontologies. You can view the entire concept space from an OWL tool such as Protege by reading in sweetAll.owl. Alternatively, these ontologies can be viewed individually. SWEET 2.3 consists of nine top-level concepts/ontologies. Some of the next-level concepts are shown in the Figure. SWEET is a middle-level ontology; most users add a domain-specific ontology using the components defined here to satisfy end user needs.");
            sweet.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Chemical");
            sweet.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");  
            sweet.put(Ontology.contact_firstname, "Foo");
            sweet.put(Ontology.contact_lastname, "Bar");
            sweet.put(Ontology.contact_email, "foo@bar.com");
            sweet.put(Ontology.backwardCompatibility, "true");
            w8.insert(sweet);
            
            GFBioMetadataWriter w9 = new GFBioMetadataWriter(ncbi_url);
            HashMap<Ontology, String> ncbi = new HashMap<Ontology, String>();
            ncbi.put(Ontology.uri, "http://purl.bioontology.org/ontology/NCBITAXON/");
            ncbi.put(Ontology.name, "National Center for Biotechnology Information (NCBI) Organismal Classification");
            ncbi.put(Ontology.acronym, "NCBITAXON");
            ncbi.put(Ontology.description, " The NCBI Taxonomy Database is a curated classification and nomenclature for all of the organisms in the public sequence databases. ");
            ncbi.put(Ontology.domains, "http://terminologies.gfbio.org/terms/ontology#Biological");
            ncbi.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");  
            ncbi.put(Ontology.contact_firstname, "Foo");
            ncbi.put(Ontology.contact_lastname, "Bar");
            ncbi.put(Ontology.contact_email, "foo@bar.com");
            ncbi.put(Ontology.backwardCompatibility, "true");
            w9.insert(ncbi);
           
    		//Some more tests for adding persons and custom languages (not yet implemented)				
    		/*HashMap<Person, String> map = new HashMap<Person, String>();
    		map.put(Person.firstName, "Peter");
    		map.put(Person.lastName, "Müller");
    		map.put(Person.eMail, "pe@example.com");
    		map.put(Person.phoneNumber, "1452563");
    		map.put(Person.faxNumber, "1452489");
    		map.put(Person.role, "Creator"); 
    		w.addPerson(map);			
    		HashMap<OntologyLanguage, String> lmap = new HashMap<OntologyLanguage, String>();
    		lmap.put(OntologyLanguage.acronym, "OWL_custom");
    		lmap.put(OntologyLanguage.description, "A language");
    		lmap.put(OntologyLanguage.documentation, "http://www.w3.org/TR/owl-semantics/");
    		lmap.put(OntologyLanguage.name, "Web Ontology Language");
    		w.addLanguage(lmap);
    		w.saveOntology();*/
		}
	}
}
