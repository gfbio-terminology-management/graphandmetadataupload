package org.gfbio.tests;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.gfbio.enums.Ontology;
import org.gfbio.metadata.GFBioMetadataWriter;
import org.gfbio.upload.GFBioOntologyUploader;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

public class PipelineTest {        
    private static String hostURL = "jdbc:virtuoso://localhost:1111";
    private static String userName = "webservice";
    private static String password = "E6:`7k#bzeTu9'W";
    private static final Logger LOGGER = Logger.getLogger(PipelineTest.class);

    public static void main(String[] args){

        //Cleaning up virtuoso for testing
        //----------------------------------------------------------------------------
        VirtGraph set = new VirtGraph (hostURL, userName, password);
        String text = "CLEAR GRAPH <http://purl.gfbio.org/Metadata>";
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (text, set);
        vqe.execSelect();
        //----------------------------------------------------------------------------
         
        //Step 1 (Precondition): Input of Metadata about the new Ontology.
        //----------------------------------------------------------------
        //Step 2: Calculation of Metrics about the new Ontology.
        //Step 3: Insertion of Metadata and Metrics into Metadatagraph
        
        String base_folder_url = "file://"+args[0];
        String geo_url = base_folder_url+"GeographicRegion.rdf";
        GFBioMetadataWriter w3 = new GFBioMetadataWriter(geo_url);
        HashMap<Ontology, String> geo = new HashMap<Ontology, String>();
        geo.put(Ontology.uri, "http://rs.tdwg.org/ontology/voc/GeographicRegion");
        geo.put(Ontology.name, "TDWG Geographic Region Ontology");
        geo.put(Ontology.acronym, "GEOREGION");
        geo.put(Ontology.description, "Ontology describing a controlled vocabulary for all of the "
            + "TDWG regions level 1 to 4 described at http://www.nhm.ac.uk/hosted_sites/tdwg/geo2.htm "
            + "and http://www.kew.org/gis/tdwg/ and collection identifiers. ");
        geo.put(Ontology.creation_date, "08/15/2007 ");
        geo.put(Ontology.domains, "http://purl.org/gfbio/metadata-schema#Geographical");
        geo.put(Ontology.ontologyLanguage, "http://omv.ontoware.org/ontology#OWL");
        geo.put(Ontology.definition, "base:definition");
        w3.insert(geo);
        LOGGER.info("Inserting finished.");
       //Step 4: Upload of Ontologyfile into Virtuoso
        LOGGER.info("Uploading terminology into Virtuoso.");
        GFBioOntologyUploader uploader = new GFBioOntologyUploader();
        uploader.upload(args[0] +"GeographicRegion.rdf", "GEOREGION");
        LOGGER.info("Uploading finished.");
        
       //Step 5: Indexing of newly added graph 
        LOGGER.info("Starting indexing of new graph.");
        uploader.index();
        LOGGER.info("Indexing finished!");
    }
}
