package org.gfbio.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.UnloadableImportException;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.util.FileUtils;

public class GFBioOntologyUploader{ 

    private String hostURL = "jdbc:virtuoso://localhost:1111";
	private String userName = "dba";
	private String password = "dba";
	private VirtuosoQueryExecution vqe;
	private String uriPrefix = "http://terminologies.gfbio.org/terms/";
	private VirtGraph set;

	public GFBioOntologyUploader(){
		this.set = new VirtGraph (hostURL, userName, password);
	}
	
	public GFBioOntologyUploader(String ip, String username, String password){
		this.hostURL = "jdbc:virtuoso://" + ip;
		this.userName = username;
		this.password = password;
		this.set = new VirtGraph (hostURL, userName, password);
	}

	/**
	 * Upload an ontology to into virtuoso
	 * @param file path fo the file (r.g. /home/foo/bar.bco) ! Make sure that virtuoso can access this directory !
	 * @param acronym acronym from the ontology
	 */
	public void upload(String file, String acronym) {
		String path = FileUtils.getDirname(file);
		String name = FileUtils.getBasename(file);
		clearUploadTable(file);
		String fillTableQuery = "ld_dir_all ('" + path + "', '" + name + "', '"
				+ uriPrefix + acronym.toUpperCase() + "')";
		String uploadQUery = "rdf_loader_run ()";
		executeSQL(fillTableQuery);
		executeSQL(uploadQUery);

		loadImports(file, acronym);
	}

	/**
	 * Clear all entries in DB.DBA.load_list for the given file to make it uploadable
	 * @param file file path
	 * @return
	 */
	private boolean clearUploadTable(String file){
		return executeSQL("delete from DB.DBA.load_list where ll_file = '" + file + "'") != null;
	}

	/**
	 * Execute an sql query
	 * @param sql query
	 * @return
	 */
	private ResultSet executeSQL(String sql){
		ResultSet rs = null;
		try {
			Class.forName("virtuoso.jdbc4.Driver");
			Connection conn = DriverManager.getConnection(hostURL, userName,password);
			PreparedStatement st;
			st = conn.prepareStatement(sql);
			rs = st.executeQuery();
			st.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return rs;
	}

	/**
	 * This method loads the imports for a given ontology into Virtuoso.
	 * 
	 * @param ontology the local ontology filename
	 * @param graphName name of the graph
	 */
	private void loadImports(String ontology, String graphName) {
		ontology = "file:///" + ontology;
		String[] array = ontology.split("\\.");
		String fileExtension = array[1];
		if (fileExtension.equals("owl")){
			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
			IRI iri = IRI.create(ontology);
			try {
				OWLOntology onto = manager.loadOntologyFromOntologyDocument(iri);
				Set<OWLOntology> imports = manager.getImports(onto);
				// for each import, fire a SPARQL query that uploads this import into Virtuoso
				for (OWLOntology importOnto : imports) {
					String sparql = "LOAD <" + importOnto.getOntologyID().getOntologyIRI() + "> INTO GRAPH <"+uriPrefix+graphName+">";
				    vqe = VirtuosoQueryExecutionFactory.create (sparql, set);
				    vqe.execSelect();
				}
			} catch (UnloadableImportException e) {
				e.printStackTrace();
			} catch (OWLOntologyCreationException e) {
				e.printStackTrace();
			}
		}
		else {
			
		}
	}

	public void index(){
	    try {
            Class.forName("virtuoso.jdbc4.Driver");
            Connection conn = DriverManager.getConnection(hostURL,userName,password);
            PreparedStatement st;
            st = conn.prepareStatement("DB.DBA.VT_INC_INDEX_DB_DBA_RDF_OBJ ()");
            st.execute();
            st.close();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	
	public static void main(String[] args){
	    GFBioOntologyUploader u = new GFBioOntologyUploader();
	    u.upload("/usr/local/var/lib/virtuoso/db/sweetAll.owl", "SWEET");
	}
}
