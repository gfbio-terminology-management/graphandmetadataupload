package org.gfbio.metadata;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gfbio.enums.Ontology;
import org.gfbio.terminologies.metrics.GFBioMetricsCalculator;
import org.gfbio.terminologies.metrics.GFBioOWLMetricsCalculator;
import org.gfbio.terminologies.metrics.GFBioSKOSMetricsCalculator;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;
import virtuoso.jena.driver.VirtuosoUpdateFactory;
import virtuoso.jena.driver.VirtuosoUpdateRequest;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

/**
 * This class inserts the extracted Metadata and metrics of an ontology into a
 * Virtuoso graph.
 * 
 * @author Alexandra la Fleur
 * 
 */
public class GFBioMetadataWriter {

	private static String hostURL = "jdbc:virtuoso://localhost:1111";
	private static String userName = "webservice";
	private static String password = "E6:`7k#bzeTu9'W";
	private VirtuosoQueryExecution vqe;
	private String uriPrefix = "http://terminologies.gfbio.org/terms/";
	private String ontology_url;
	private VirtGraph set;
	private StringBuilder sb = new StringBuilder();

	private String classesNumber = "numberOfClasses";
	private String individualNumber = "numberOfIndividuals";
	private String maximumNumberOfChildren = "maximumNumberOfChildren";
	private String maximumDepth = "maximumDepth";
	private String twentyfiveChildren = "classesWithMoreThan25Children";
	private String averageNumberOfChildren = "averageNumberOfChildren";
	private String propertyNumber = "numberOfProperties";
	private String singleChild = "classesWithASingleChild";
	private String classesWithoutDefinition = "classesWithoutDefinition";
	private String numberOfLeaves = "numberOfLeaves";
	private String classesWithoutLabel = "classesWithoutLabel";
	private String classesWithMoreThan1Parent = "classesWithMoreThan1Parent";
	private String descriptionLogicName = "descriptionLogicName";
	private final String owl = "http://omv.ontoware.org/2005/05/ontology#OWL";
	private final String skos = "http://terminologies.gfbio.org/terms/ontology#SKOS";

	public GFBioMetadataWriter(String ontology_url) {
		set = new VirtGraph(hostURL, userName, password);
		this.ontology_url = ontology_url;
	}

	public void insert(HashMap<Ontology, String> mp) {
		HashMap<String, Object> metricsMap = new HashMap<String, Object>();
		GFBioMetricsCalculator cal = null;
		switch (mp.get(Ontology.ontologyLanguage)) {
		case owl:
			cal = new GFBioOWLMetricsCalculator(ontology_url);
			break;
		case skos:
			cal = new GFBioSKOSMetricsCalculator(ontology_url);
			break;
		}
		String def = mp.get(Ontology.definition) != null? mp.get(Ontology.definition) : "http://www.w3.org/2004/02/skos/core#definition";
		String label = mp.get(Ontology.definition) != null? mp.get(Ontology.definition) : "http://www.w3.org/2000/01/rdf-schema#label";
		cal.setDefinition(def);
		cal.setLabel(label);
		cal.calculateMetrics();
		metricsMap.put(classesNumber, cal.getClassesCount());
		metricsMap.put(individualNumber, cal.getIndividualsCount());
		metricsMap.put(propertyNumber, cal.getPropertiesCount());
		metricsMap.put(maximumDepth, cal.getMaxDepth());
		metricsMap.put(maximumNumberOfChildren, cal.getMaxChildren());
		metricsMap.put(averageNumberOfChildren, cal.getAvgChildren());
		metricsMap.put(singleChild, cal.getSingleChild());
		metricsMap.put(twentyfiveChildren, cal.getMore25Children());
		metricsMap.put(classesWithoutDefinition,
				cal.getClassesWithoutDefinition());
		metricsMap.put(descriptionLogicName, cal.getDescriptionLogicName());
		metricsMap.put(numberOfLeaves, cal.getNumberOfLeaves());
		metricsMap.put(classesWithoutLabel, cal.getClasesWithoutLabel());
		metricsMap.put(classesWithMoreThan1Parent, cal.getClassesWithMoreThan1Parent());

		this.insert(mp, metricsMap);
	}

	/**
	 * Main method to insert metadata and metrics in a graph.
	 * 
	 * @param mp
	 *            metadata map
	 * @param metricsMap
	 */
	public void insert(HashMap<Ontology, String> mp,
			HashMap<String, Object> metricsMap) {

		String oldVersion = null;
		int hash = mp.get(Ontology.acronym).hashCode();

		if (existsPreviousVersion(mp.get(Ontology.acronym))) {
			// delete gfbio:graph Property
			deleteGraphLink(mp.get(Ontology.acronym));
			// rehash ontology identifier for old ontology
			oldVersion = changeOldOntologyIdentifier(mp.get(Ontology.acronym));
		}
		int hashPerson = 0;
		int hashOrga = 0;
		if(mp.get(Ontology.contact_firstname) != null && mp.get(Ontology.contact_lastname) != null){
		    hashPerson = createContact(mp.get(Ontology.contact_firstname),mp.get(Ontology.contact_lastname),
    				mp.get(Ontology.contact_email));
		}
	    if(mp.get(Ontology.contact_organisation) != null){
	        hashOrga = createContact(mp.get(Ontology.contact_organisation), hashPerson);
	    }

		sb.append("INSERT INTO GRAPH <" + uriPrefix + "Metadata>" + " { <"
				+ uriPrefix + hash + "> a omv:Ontology.");

		for (Ontology att : Ontology.values()) {
			if (mp.get(att) != null && mp.get(att).contains("'")) {
				String badFormattedValue = mp.get(att);
				String formattedvalue = badFormattedValue.replace("'", "\\'");
				mp.put(att, formattedvalue);
			}

			// Data Properties
			if (!att.name().equals("ontologyLanguage")
					&& !att.name().equals("uri")
					&& !att.name().equals("domain")
					&& !att.name().equals("homepage")
					&& !att.name().equals("documentation")
					&& !att.name().equals("release_date")
					&& !att.name().equals("creation_date")
					&& !att.name().equals("formality_level")) {
				if (att.name().equals("definition")) {
					if (mp.get(att) == null) {
						sb.append("<" + uriPrefix + hash + "> gfbio:"
								+ att.name() + " skos:definition .");
					} else {
						sb.append("<" + uriPrefix + hash + "> gfbio:"
								+ att.name() + " <" + mp.get(att) + "> .");
					}
				} else if (att.name().equals("synonym")) {
					if (mp.get(att) == null) {
						sb.append("<" + uriPrefix + hash + "> gfbio:"
								+ att.name() + " skos:altLabel.");
					} else {
						sb.append("<" + uriPrefix + hash + "> gfbio:"
								+ att.name() + " <" + mp.get(att) + ">.");
					}
				} else if (att.name().equals("label")) {
					if (mp.get(att) == null) {
					    String label = "rdfs:label";
					    switch (mp.get(Ontology.ontologyLanguage)) {
				        case owl:
				            label = "rdfs:label";
				            break;
				        case skos:
				            label = "skos:prefLabel";
				            break;
				        }
					    
						sb.append("<" + uriPrefix + hash + "> gfbio:"
								+ att.name() +label+".");
					} else {
						sb.append("<" + uriPrefix + hash + "> gfbio:"
								+ att.name() + " <" + mp.get(att) + ">.");
					}
				} else if (mp.get(att) != null && !mp.get(att).trim().isEmpty()) {
					if (att.name().equals("version")
							|| att.name().equals("name")
							|| att.name().equals("acronym")
							|| att.name().equals("description")
							|| att.name().equals("keywords")
							|| att.name().equals("naturalLanguage")
							|| att.name().equals("status")) {
						sb.append("<" + uriPrefix + hash + "> omv:"
								+ att.name() + " '" + mp.get(att) + "'.");
					}
				} else {
					checkNullOrEmptyString(mp.get(att), att.name(), hash);
				}
			}
		}

		checkNullOrEmptyDate(mp.get(Ontology.creation_date),
				"omv:creationDate", hash);
		checkNullOrEmptyDate(mp.get(Ontology.release_date), "gfbio:relaseDate",
				hash);
		checkNullOrEmptyResource(mp.get(Ontology.uri), "omv:uri", hash);
		checkNullOrEmptyResource(mp.get(Ontology.homepage), "gfbio:homepage",
				hash);
		if(hashPerson != 0){
		    checkNullOrEmptyResource(uriPrefix + hashPerson, "gfbio:hasContact",
				hash);
		}
	    if(hashOrga != 0){
	            checkNullOrEmptyResource(uriPrefix + hashOrga, "gfbio:hasContact",
	                hash);
	    }
		checkNullOrEmptyResource(mp.get(Ontology.documentation),
				"omv:documentation", hash);
		checkNullOrEmptyResource(uriPrefix + mp.get(Ontology.acronym),
				"gfbio:graph", hash);
	    checkNullOrEmptyResource(mp.get(Ontology.abbrevation),
              "gfbio:abbrevation", hash);

		sb.append("<" + uriPrefix + hash + "> omv:" + classesNumber + " '"
				+ metricsMap.get(classesNumber) + "'.");
		sb.append("<" + uriPrefix + hash + "> omv:" + individualNumber + " '"
				+ metricsMap.get(individualNumber) + "'.");
		sb.append("<" + uriPrefix + hash + "> omv:" + propertyNumber + " '"
				+ metricsMap.get(propertyNumber) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:" + maximumDepth + " '"
				+ metricsMap.get(maximumDepth) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:" + maximumNumberOfChildren
				+ " '" + metricsMap.get(maximumNumberOfChildren) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:" + averageNumberOfChildren
				+ " '" + metricsMap.get(averageNumberOfChildren) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:" + singleChild + " '"
				+ metricsMap.get(singleChild) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:" + twentyfiveChildren
				+ " '" + metricsMap.get(twentyfiveChildren) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:"
				+ classesWithoutDefinition + " '"
				+ metricsMap.get(classesWithoutDefinition) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:" + numberOfLeaves + " '"
				+ metricsMap.get(numberOfLeaves) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:" + classesWithoutLabel
				+ " '" + metricsMap.get(classesWithoutLabel) + "'.");
		sb.append("<" + uriPrefix + hash + "> gfbio:"
				+ classesWithMoreThan1Parent + " '"
				+ metricsMap.get(classesWithMoreThan1Parent) + "'.");

		// Object Properties
		sb.append("<" + uriPrefix + hash + "> omv:hasOntologyLanguage <"
				+ mp.get(Ontology.ontologyLanguage) + ">.");

		if(mp.containsKey(Ontology.domains)){
			String[] domains = mp.get(Ontology.domains).split(",");
			for(String domain : domains){
				sb.append("<" + uriPrefix + hash + "> omv:hasDomain <" + domain + ">.");
			}
		}

		checkNullOrEmptyResource(mp.get(Ontology.formality_level),
				"omv:hasFormalityLevel", hash);

		checkNullOrEmptyString((String) metricsMap.get(descriptionLogicName),
				"gfbio:hasDlExpressivity", hash);

		sb.append("<" + uriPrefix + hash + "> omv:resourceLocator '"
				+ mp.get(Ontology.ontology_store_location) + "'.");

		// link new version to older version and set compatibility, if exists
		if (oldVersion != null) {
			linkOntologies(sb, oldVersion, "" + hash);
			if (mp.get(Ontology.backwardCompatibility).equals("true")) {
				sb.append("<" + uriPrefix + hash
						+ "> omv:isBackwardCompatibleWith <" + uriPrefix
						+ oldVersion + "> .");
			} else if (mp.get(Ontology.inCompatibility).equals("true")) {
				sb.append("<" + uriPrefix + hash + "> omv:isIncompatibleWith <"
						+ uriPrefix + oldVersion + "> .");
			}
		}

		sb.append("}");

		System.out.println(sb.toString());
		vqe = VirtuosoQueryExecutionFactory.create(sb.toString(), set);
		vqe.execSelect();
	}

	private int createContact(String contactFirstName, String contactLastName, String contactMail) {
		int hash = contactMail.hashCode();
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO GRAPH <" + uriPrefix + "Metadata>" + " { <"
				+ uriPrefix + hash + "> a omv:Person. ");
		if(contactMail != null){
    		sb.append("<" + uriPrefix + hash + "> omv:eMail '" + contactMail
    				+ "'. ");
		}
		sb.append("<" + uriPrefix + hash + "> omv:lastName '" + contactLastName
				+ "'. ");
		sb.append("<" + uriPrefix + hash + "> omv:firstName '" + contactFirstName
				+ "'} ");
		vqe = VirtuosoQueryExecutionFactory.create(sb.toString(), set);
		vqe.execSelect();
		return hash;
	}
	
	   private int createContact(String organisation, int person) {
	        int hash = organisation.hashCode();
	        StringBuilder sb = new StringBuilder();
	        sb.append("INSERT INTO GRAPH <" + uriPrefix + "Metadata>" + " { <"
	                + uriPrefix + hash + "> a omv:Organisation. ");
	        sb.append("<" + uriPrefix + hash + "> omv:hasContactPerson ' <" + uriPrefix + person + 
	            "> '. ");
	        sb.append("<" + uriPrefix + hash + "> omv:name '" + organisation
	                + "'} ");
	        vqe = VirtuosoQueryExecutionFactory.create(sb.toString(), set);
	        vqe.execSelect();
	        return hash;
	    }

	public Set<String> checkConcepts(int oldVersion,
			String newVersionFileLocation) {
		StringBuilder sb = new StringBuilder();
		String uriPrefix = "http://purl.gfbio.org/";
		sb.append("SELECT ?fileLocation from <" + uriPrefix
				+ "Metadata> WHERE {");
		sb.append("<" + uriPrefix + oldVersion
				+ "> omv:resourceLocator ?fileLocation }");
		vqe = VirtuosoQueryExecutionFactory.create(sb.toString(), set);
		ResultSet resultSet = vqe.execSelect();
		while (resultSet.hasNext()) {
			QuerySolution result = resultSet.next();
			RDFNode fileLocation = result.get("fileLocation");
			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
			IRI iri = IRI.create("file://" + fileLocation);

			OWLOntologyManager manager2 = OWLManager.createOWLOntologyManager();
			IRI iri2 = IRI.create(newVersionFileLocation);

			try {
				OWLOntology o = manager.loadOntologyFromOntologyDocument(iri);
				Set<OWLClass> classesInSignature = o.getClassesInSignature();
				OWLOntology o2 = manager2
						.loadOntologyFromOntologyDocument(iri2);
				Set<OWLClass> classesInSignature2 = o2.getClassesInSignature();
				boolean equals = classesInSignature.equals(classesInSignature2);

				Set<String> removedClasses = new HashSet<String>();

				if (!equals) {
					// check which classes are deleted in the newer version
					// --> could lead to incompatibility
					classesInSignature.removeAll(classesInSignature2);
					removedClasses = convertToStringSet(classesInSignature);
				}
				return removedClasses;

			} catch (OWLOntologyCreationException e) {
				e.printStackTrace();
			}
		}
		return new HashSet<String>();

	}

	private Set<String> convertToStringSet(Set<OWLClass> diffClasses) {
		Set<String> diffClassesSet = new HashSet<String>();
		for (OWLClass owlClass : diffClasses) {
			String iri = owlClass.getIRI().toString();
			diffClassesSet.add(iri);
		}
		return diffClassesSet;
	}

	/**
	 * Checks whether the String representation of a date is null or empty. If
	 * not so, the date is appended to StringBuilder with the given property and
	 * hash value.
	 * 
	 * @param date
	 * @param property
	 * @param hash
	 */
	private void checkNullOrEmptyDate(String date, String property, int hash) {
		if (date != null && !date.trim().isEmpty()) {
			sb.append("<" + uriPrefix + hash + "> " + property + " '" + date
					+ "'^^xsd:date .");
		}
	}

	/**
	 * Checks whether the string representation of a resource is null or empty.
	 * If not so, the resource is appended to StringBuilder with the given
	 * property and hash
	 * 
	 * @param item
	 * @param property
	 * @param hash
	 */
	private void checkNullOrEmptyResource(String item, String property, int hash) {
		if (item != null && !item.trim().isEmpty()) {
			sb.append("<" + uriPrefix + hash + "> " + property + " <" + item
					+ ">.");
		}
	}

	/**
	 * Checks whether a string is null or empty. If not so, the resource is
	 * appended to StringBuilder with the given property and hash
	 * 
	 * @param string
	 * @param property
	 * @param hash
	 */
	private void checkNullOrEmptyString(String string, String property, int hash) {
		if (string != null && !string.trim().isEmpty()) {
			sb.append("<" + uriPrefix + hash + "> gfbio:" + property + " '"
					+ string + "'.");
		}
	}

	/**
	 * changes the OntologyIdentifier for the old version by recomputing the
	 * hash value (acronym + current date of archiving)
	 * 
	 * @param acronym
	 * @return the new hash of the old version
	 */
	public String changeOldOntologyIdentifier(String acronym) {
		/*
		 * WITH <http://purl.gfbio.org/Metadata> DELETE {
		 * <http://purl.gfbio.org/285520297> ?x ?y } INSERT {
		 * <http://purl.gfbio.org/222222222> ?x ?y } WHERE {
		 * <http://purl.gfbio.org/285520297> ?x ?y }
		 */
		Date archivedDate = new Date();
		int newHash = (acronym + archivedDate.toString()).hashCode();
		int oldHash = acronym.hashCode();

		String uriPrefix = "http://purl.gfbio.org/";
		StringBuilder sb = new StringBuilder();
		sb.append("WITH <" + uriPrefix + "Metadata> ");
		sb.append("DELETE {<" + uriPrefix + oldHash + "> ?attr ?val } ");
		sb.append("INSERT {<" + uriPrefix + newHash + "> ?attr ?val } ");
		sb.append("WHERE {<" + uriPrefix + oldHash + "> ?attr ?val } ");

		VirtuosoUpdateRequest update = VirtuosoUpdateFactory.create(
				sb.toString(), set);
		update.exec();
		return "" + newHash;
	}

	/**
	 * deletes the gfbio:graph property from the old Ontology
	 * 
	 * @param oldOntologyID
	 * @param oldOntologyGraph
	 */
	private void deleteGraphLink(String acronym) {
		int hash = acronym.hashCode();
		String uriPrefix = "http://purl.gfbio.org/";
		StringBuilder sb = new StringBuilder();
		sb.append("WITH <" + uriPrefix + "Metadata> ");
		sb.append("DELETE { <" + uriPrefix + hash + "> gfbio:graph <"
				+ uriPrefix + acronym + "> }");
		VirtuosoUpdateRequest delete = VirtuosoUpdateFactory.create(
				sb.toString(), set);
		delete.exec();
	}

	/**
	 * returns if a previous version for this acronym exists
	 * 
	 * @param acronym
	 *            the acronym of the ontology
	 * @return true if a previous version exists, false otherwise
	 */
	public boolean existsPreviousVersion(String acronym) {
		int hash = acronym.hashCode();
		String uriPrefix = "http://purl.gfbio.org/";
		StringBuilder sb = new StringBuilder();
		sb.append("ASK FROM <" + uriPrefix + "Metadata> WHERE { ");
		sb.append("<" + uriPrefix + hash + "> omv:acronym '" + acronym + "' }");
		vqe = VirtuosoQueryExecutionFactory.create(sb.toString(), set);
		boolean existsPreviousVersion = vqe.execAsk();
		return existsPreviousVersion;
	}

	/**
	 * The method is called after the user has uploaded an ontology and the data
	 * is stored in a ticket.<br>
	 * We extract the old ontology metadata to display a compatibility view in
	 * the admin view and compare the data with each other.
	 * 
	 * @param acronym
	 * @return old ontology data as a Map
	 */
	public Map<String, Object> getOldOntoData(String acronym) {
		Map<String, Object> oldData = new HashMap<String, Object>();
		int hash = acronym.hashCode();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * from <" + uriPrefix + "Metadata> WHERE {");
		sb.append("<" + uriPrefix + hash
				+ "> omv:numberOfClasses ?numberOfClasses. ");
		sb.append("<" + uriPrefix + hash
				+ "> omv:numberOfIndividuals ?numberOfIndividuals. ");
		sb.append("<" + uriPrefix + hash
				+ "> omv:numberOfProperties ?numberOfProperties. ");
		sb.append("<" + uriPrefix + hash
				+ "> gfbio:maximumDepth ?maximumDepth. ");
		sb.append("<" + uriPrefix + hash
				+ "> gfbio:maximumNumberOfChildren ?maximumNumberOfChildren. ");
		sb.append("<" + uriPrefix + hash
				+ "> gfbio:averageNumberOfChildren ?averageNumberOfChildren. ");
		sb.append("<"
				+ uriPrefix
				+ hash
				+ "> gfbio:numberOfClassesWithASingleChild ?numberOfClassesWithASingleChild. ");
		sb.append("<"
				+ uriPrefix
				+ hash
				+ "> gfbio:classeswithMoreThan25Children ?classeswithMoreThan25Children. ");
		sb.append("<"
				+ uriPrefix
				+ hash
				+ "> gfbio:numberOfClassesWithoutDefinition ?numberOfClassesWithoutDefinition. ");
		sb.append("<" + uriPrefix + hash
				+ "> gfbio:numberOfLeaves ?numberOfLeaves. ");
		sb.append("<" + uriPrefix + hash
				+ "> gfbio:classesWithoutLabel ?classesWithoutLabel. ");
		sb.append("<"
				+ uriPrefix
				+ hash
				+ "> gfbio:classesWithMoreThan1Parent ?classesWithMoreThan1Parent} ");
		vqe = VirtuosoQueryExecutionFactory.create(sb.toString(), set);
		ResultSet result = vqe.execSelect();
		while (result.hasNext()) {
			QuerySolution data = result.next();
			oldData.put("numberOfClasses", data.get("numberOfClasses")
					.toString());
			oldData.put("numberOfIndividuals", data.get("numberOfIndividuals")
					.toString());
			oldData.put("numberOfProperties", data.get("numberOfProperties")
					.toString());
			oldData.put("maximumDepth", data.get("maximumDepth").toString());
			oldData.put("maximumNumberOfChildren",
					data.get("maximumNumberOfChildren").toString());
			oldData.put("averageNumberOfChildren",
					data.get("averageNumberOfChildren").toString());
			oldData.put("numberOfClassesWithASingleChild",
					data.get("numberOfClassesWithASingleChild").toString());
			oldData.put("classeswithMoreThan25Children",
					data.get("classeswithMoreThan25Children").toString());
			oldData.put("numberOfClassesWithoutDefinition",
					data.get("numberOfClassesWithoutDefinition").toString());
			oldData.put("numberOfLeaves", data.get("numberOfLeaves").toString());
			oldData.put("classesWithoutLabel", data.get("classesWithoutLabel")
					.toString());
			oldData.put("classesWithMoreThan1Parent",
					data.get("classesWithMoreThan1Parent").toString());
		}
		return oldData;
	}

	/**
	 * links the newOntology with gfbio:hasPriorVersion to the oldOntology
	 * 
	 * @param oldOntology
	 * @param newOntology
	 */
	private void linkOntologies(StringBuilder sb, String oldOntology,
			String newOntology) {
		sb.append("<" + uriPrefix + newOntology + "> omv:hasPriorVersion <"
				+ uriPrefix + oldOntology + "> .");
	}

}
