package org.gfbio.metadata;

import java.util.ArrayList;

/**
 * Class to be used for adding persons and their authored ontologies
 * 
 *
 */
public class OntologyList extends ArrayList<String>{
    public OntologyList(String s) throws NullPointerException{
        super();
        
        if (s.length() != 0)
        {
        if (!s.contains(",")) add(s.trim());
        else
        for (String v : s.split(",")) {
            try {
                add(v.trim());
            } catch (Exception ex) {
            }
        }
        }
        
    }
}