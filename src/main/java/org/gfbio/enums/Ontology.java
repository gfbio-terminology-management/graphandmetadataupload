package org.gfbio.enums;

/**
 * An enumeration for Ontology data.
 * 
 * @author Alexandra La Fleur
 * @author Vincent Bohlen
 *
 */
public enum Ontology{
	uri, name, acronym, description, ontologyLanguage, creation_date, definition, domains, synonym, label, 
	version, release_date, status, keywords, creators, contributors, naturalLanguage, contact_firstname, contact_lastname,
	contact_email, contact_organisation, homepage, documentation, formality_level, ontology_store_location, ontology_file, 
	backwardCompatibility, inCompatibility, dlExpressivity, abbrevation, symbol
}
