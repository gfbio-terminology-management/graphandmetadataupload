package org.gfbio.enums;

/**
 * An enumeration for Ontologylanguage data.
 * 
 */
public enum OntologyLanguage{
	name, acronym, description, documentation
}