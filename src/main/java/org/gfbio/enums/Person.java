package org.gfbio.enums;

/**
 * An enumeration for Person data.
 * 
 * @author Alexandra la Fleur
 *
 */
public enum Person{
	firstName, lastName, eMail, phoneNumber, faxNumber, role
}